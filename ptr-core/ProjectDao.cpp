#include "ProjectDao.h"

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QVariant>

#include "Project.h"
#include "DatabaseManager.h"

using namespace std;

ProjectDao::ProjectDao(QSqlDatabase& database) :
    m_database(database)
{
}

void ProjectDao::init() const
{
    if (!m_database.tables().contains("projects")) {
        QSqlQuery query(m_database);
        query.exec("CREATE TABLE projects (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, is_active BOOLEAN DEFAULT 0)");
        DatabaseManager::debugQuery(query);
        query.exec(R"(INSERT INTO projects (name, is_active) VALUES ('DefaultProject', 1))");
    }
}

void ProjectDao::addProject(Project& project) const
{
    QSqlQuery query(m_database);
    query.prepare("INSERT INTO projects (name) VALUES (:name)");
    query.bindValue(":name", project.name());
    query.exec();
    project.setId(query.lastInsertId().toInt());
    DatabaseManager::debugQuery(query);
}

void ProjectDao::updateProjectName(const Project &project) const
{
    QSqlQuery query(m_database);
    query.prepare("UPDATE projects SET name = (:name) WHERE id = (:id)");
    query.bindValue(":name", project.name());
    query.bindValue(":id", project.id());
    query.exec();
    DatabaseManager::debugQuery(query);
}

void ProjectDao::removeProject(int id) const
{
    QSqlQuery query(m_database);
    query.prepare("DELETE FROM projects WHERE id = (:id)");
    query.bindValue(":id", id);
    query.exec();
    DatabaseManager::debugQuery(query);
}

void ProjectDao::updateActiveProject(const Project &project) const
{
    resetActiveProject();
    QSqlQuery query(m_database);
    query.prepare("UPDATE projects SET is_active = 1 WHERE id = (:id)");
    query.bindValue(":id", project.id());
    query.exec();
    DatabaseManager::debugQuery(query);
}

std::optional<int> ProjectDao::activeProject() const
{
    QSqlQuery query("SELECT id FROM projects WHERE is_active = 1", m_database);
    query.exec();

    if (query.next())
        return query.value("id").toInt();
    else
        return std::optional<int>();
}

std::vector<Project> ProjectDao::projects() const
{
    QSqlQuery query("SELECT * FROM projects", m_database);
    query.exec();

    vector<Project> list;

    while(query.next()) {
        Project project;
        project.setId(query.value("id").toInt());
        project.setName(query.value("name").toString());
        project.setActive(query.value("is_active").toBool());
        list.push_back(project);
    }
    return list;
}

void ProjectDao::resetActiveProject() const
{
    QSqlQuery query(m_database);
    query.prepare("UPDATE projects SET is_active = 0 WHERE is_active = 1");
    query.exec();
    DatabaseManager::debugQuery(query);
}
