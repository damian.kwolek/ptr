#ifndef PROJECTDAO_H
#define PROJECTDAO_H

#include <memory>
#include <vector>
#include <optional>

class QSqlDatabase;
class Project;

class ProjectDao
{
public:
    ProjectDao(QSqlDatabase& database);
    void init() const;

    void addProject(Project& project) const;
    void updateProjectName(const Project& project) const;
    void removeProject(int id) const;
    void updateActiveProject(const Project& project) const;
    std::optional<int> activeProject() const;
    std::vector<Project> projects() const;

private:
    void resetActiveProject() const;

    QSqlDatabase& m_database;
};

#endif // PROJECTDAO_H
