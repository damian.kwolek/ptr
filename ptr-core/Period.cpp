#include "Period.h"

Period::Period(const QString &processName, const QString &windowTitle, QDateTime periodBegin, QDateTime periodEnd) :
    m_id(-1), m_projectId(-1),
    m_processName(processName), m_windowTitle(windowTitle),
    m_periodBegin(periodBegin), m_periodEnd(periodEnd)
{

}

Period::Period(int id, int projectId, const QString &processName, const QString &windowTitle, QDateTime periodBegin, QDateTime periodEnd) :
    Period(processName, windowTitle, periodBegin, periodEnd)
{
    m_id = id;
    m_projectId = projectId;
}

int Period::id() const
{
    return m_id;
}

void Period::setId(int id)
{
    m_id = id;
}

int Period::projectId() const
{
    return m_projectId;
}

void Period::setProjectId(int projectId)
{
    m_projectId = projectId;
}

QString Period::processName() const
{
    return m_processName;
}

QString Period::windowTitle() const
{
    return m_windowTitle;
}

QDateTime Period::periodBegin() const
{
    return m_periodBegin;
}

QDateTime Period::periodEnd() const
{
    return m_periodEnd;
}
