#include "StatisticRow.h"


StatisticRow::StatisticRow() :
    m_groupTxt{},
    m_count{0},
    m_sum{0},
    m_avg{0.0f}
{

}

StatisticRow::StatisticRow(QString groupTxt, unsigned count, unsigned sum, float avg) :
    m_groupTxt{groupTxt},
    m_count{count},
    m_sum{sum},
    m_avg{avg}
{

}
