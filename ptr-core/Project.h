#ifndef PROJECT_H
#define PROJECT_H

#include <QString>

#include "ptr-core_global.h"

class PTRCORESHARED_EXPORT Project
{
public:
    explicit Project(const QString& name = "");

    int id() const;
    void setId(int id);
    QString name() const;
    void setName(const QString& name);
    bool isActive() const;
    void setActive(bool active = true);

private:
    int m_id;
    QString m_name;
    bool m_isActive;
};

#endif // PROJECT_H
