#ifndef PROJECTMODEL_H
#define PROJECTMODEL_H

#include <QAbstractListModel>
#include <QHash>
#include <vector>
#include <memory>

#include "ptr-core_global.h"
#include "Project.h"
#include "DatabaseManager.h"

class PTRCORESHARED_EXPORT ProjectModel : public QAbstractListModel
{
    Q_OBJECT
public:

    enum Roles {
        IdRole = Qt::UserRole + 1,
        NameRole,
        IsActiveRole
    };

    ProjectModel(QObject* parent = nullptr);
    ~ProjectModel() override;

    QModelIndex addProject(const Project& project);
    void setActiveProject(const QModelIndex& index);
    std::optional<QModelIndex> activeProjectIndex() const;
    std::optional<int> activeProjectId() const;
    std::optional<int> projectId(const QModelIndex& index) const;

    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
    bool setData(const QModelIndex& index, const QVariant& value, int role) override;
    bool removeRows(int row, int count, const QModelIndex& parent) override;

public slots:
    void trackingChanged(bool tracking);

private:
    bool isIndexValid(const QModelIndex& index) const;
    void clearActiveProject();

private:
    DatabaseManager& m_db;
    std::vector<Project> m_projects;
    bool m_setActiveEnable;
};

#endif // PROJECTMODEL_H
