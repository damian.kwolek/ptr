#ifndef PERIODMODEL_H
#define PERIODMODEL_H

#include <memory>
#include <vector>

#include <QAbstractListModel>

#include "ptr-core_global.h"
#include "Period.h"

class Project;
class DatabaseManager;
class ProjectModel;

class PTRCORESHARED_EXPORT PeriodModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    enum Columns {
        PeriodBeginColumn,
        PeriodEndColumn,
        PeriodTimeColumn,
        ProcessNameColumn,
        WindowTitleColumn
    };

    PeriodModel(QObject* parent = nullptr);

    QModelIndex addPeriod(const Period& period);
    void loadPeriods(QDateTime from, QDateTime to, int projectId);

    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    int columnCount(const QModelIndex& parent = QModelIndex()) const override;
    QVariant data(const QModelIndex& index, int role) const override;
    bool removeRows(int row, int count, const QModelIndex& parent) override;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;

private:
    bool isIndexValid(const QModelIndex& index) const;

    DatabaseManager& m_db;
    std::vector<Period> m_periods;
};

#endif // PERIODMODEL_H
