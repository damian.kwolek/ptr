#ifndef STATSMODEL_H
#define STATSMODEL_H

#include <QAbstractTableModel>
#include "Period.h"
#include "StatisticRow.h"

class DatabaseManager;

class StatsModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    enum Columns {
        GroupColumn,
        CountColumn,
        SummaryTimeColumn,
        AverageTimeColumn
    };

    explicit StatsModel(QObject *parent = nullptr);

    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    void loadStatistics(QDateTime from, QDateTime to, int projectId, int groupBy);

private:
    bool isIndexValid(const QModelIndex& index) const;

    DatabaseManager& m_db;
    std::vector<StatisticRow> m_statistics;
};

#endif // STATSMODEL_H
