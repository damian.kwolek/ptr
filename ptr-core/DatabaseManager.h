#ifndef DATABASEMANAGER_H
#define DATABASEMANAGER_H

#include <memory>

#include <QString>

#include "ProjectDao.h"
#include "PeriodDao.h"

class QSqlQuery;
class QSqlDatabase;

const QString DATABASE_FILENAME = "ptr.db";

class DatabaseManager
{
public:
    static void debugQuery(const QSqlQuery& query);

    static DatabaseManager& instance();
    ~DatabaseManager();

protected:
    DatabaseManager(const QString& path = DATABASE_FILENAME);

private:
    std::unique_ptr<QSqlDatabase> m_database;

public:
    const ProjectDao projectDao;
    const PeriodDao periodDao;
};

#endif // DATABASEMANAGER_H
