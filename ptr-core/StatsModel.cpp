#include "StatsModel.h"
#include "DatabaseManager.h"
#include <cmath>

StatsModel::StatsModel(QObject *parent)
    : QAbstractTableModel(parent),
      m_db(DatabaseManager::instance())
{
}

QVariant StatsModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole && orientation == Qt::Horizontal) {
        switch (section) {
        case GroupColumn:
            return QString(tr("Group"));
        case CountColumn:
            return QString(tr("Count"));
        case SummaryTimeColumn:
            return QString(tr("Summary time"));
        case AverageTimeColumn:
            return QString(tr("Average time"));
        }
    }
    return QVariant();
}

int StatsModel::rowCount(const QModelIndex& /*parent*/) const
{
    return static_cast<int>(m_statistics.size());
}

int StatsModel::columnCount(const QModelIndex& /*parent*/) const
{
    return 4;
}

QVariant StatsModel::data(const QModelIndex &index, int role) const
{
    if (!isIndexValid(index))
        return QVariant();

    if (role != Qt::DisplayRole && role != Qt::TextAlignmentRole)
        return QVariant();

    size_t row = static_cast<size_t>(index.row());

    if (role == Qt::DisplayRole) {
        switch (index.column()) {
        case GroupColumn:
            return m_statistics[row].groupTxt();
        case CountColumn:
            return m_statistics[row].count();
        case SummaryTimeColumn:
            return QDateTime::fromTime_t(m_statistics[row].sum()).toUTC().toString("hh:mm:ss");
        case AverageTimeColumn:
            return QDateTime::fromTime_t(static_cast<unsigned>(std::round(m_statistics[row].avg()))).toUTC().toString("hh:mm:ss");
        }
    }
    else if (Qt::TextAlignmentRole)
        return index.column() == GroupColumn ? Qt::AlignLeft : Qt::AlignCenter;

    return QVariant();
}

void StatsModel::loadStatistics(QDateTime from, QDateTime to, int projectId, int groupBy)
{
    beginResetModel();
    m_statistics = DatabaseManager::instance().periodDao.statistics(from, to, projectId, groupBy);
    endResetModel();
}

bool StatsModel::isIndexValid(const QModelIndex &index) const
{
    return !(index.row() < 0 || index.row() >= rowCount() || index.column() < 0 || index.column() >= columnCount() || !index.isValid());
}
