#-------------------------------------------------
#
# Project created by QtCreator 2019-06-16T15:08:40
#
#-------------------------------------------------

QT       += sql

TARGET = ptr-core
TEMPLATE = lib
CONFIG += lib c++17

DEFINES += PTRCORE_LIBRARY

SOURCES += \
    DatabaseManager.cpp \
    Project.cpp \
    ProjectModel.cpp \
    ProjectDao.cpp \
    Period.cpp \
    PeriodDao.cpp \
    PeriodModel.cpp \
    StatsModel.cpp \
    StatisticRow.cpp

HEADERS += \
    ptr-core_global.h \
    DatabaseManager.h \
    Project.h \
    ProjectModel.h \
    ProjectDao.h \
    Period.h \
    PeriodDao.h \
    PeriodModel.h \
    StatsModel.h \
    StatisticRow.h
