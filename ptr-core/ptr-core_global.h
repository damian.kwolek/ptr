#ifndef PTRCORE_GLOBAL_H
#define PTRCORE_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(PTRCORE_LIBRARY)
#  define PTRCORESHARED_EXPORT Q_DECL_EXPORT
#else
#  define PTRCORESHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // PTRCORE_GLOBAL_H
