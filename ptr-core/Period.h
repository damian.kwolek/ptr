#ifndef PERIOD_H
#define PERIOD_H

#include <QString>
#include <QDateTime>

#include "ptr-core_global.h"

class PTRCORESHARED_EXPORT Period
{
public:
    explicit Period(const QString& processName, const QString& windowTitle, QDateTime periodBegin, QDateTime periodEnd);
    explicit Period(int id, int projectId, const QString& processName, const QString& windowTitle, QDateTime periodBegin, QDateTime periodEnd);


    void setId(int id);
    void setProjectId(int projectId);

    int id() const;
    int projectId() const;
    QString processName() const;
    QString windowTitle() const;
    QDateTime periodBegin() const;
    QDateTime periodEnd() const;

private:
    int m_id;
    int m_projectId;
    QString m_processName;
    QString m_windowTitle;
    QDateTime m_periodBegin;
    QDateTime m_periodEnd;
};

#endif // PERIOD_H
