#ifndef PERIODDAO_H
#define PERIODDAO_H

#include <memory>
#include <vector>

class QSqlDatabase;
class Period;
class StatisticRow;
class QDateTime;

class PeriodDao
{
public:
    explicit PeriodDao(QSqlDatabase& database);
    void init() const;

    void addPeriodInProject(int projectId, Period& period) const;
    void removePeriod(int id) const;
    void removePeriodsForProject(int projectId) const;
    std::vector<Period> periods(QDateTime from, QDateTime to, int projectId) const;
    std::vector<StatisticRow> statistics(QDateTime from, QDateTime to, int projectId, int groupBy) const;

private:
    QSqlDatabase& m_database;
};

#endif // PERIODDAO_H
