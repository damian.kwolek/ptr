#include "Project.h"

Project::Project(const QString& name) :
    m_id(-1),
    m_name(name),
    m_isActive(false)
{
}

int Project::id() const
{
    return m_id;
}

void Project::setId(int id)
{
    m_id = id;
}

QString Project::name() const
{
    return m_name;
}

void Project::setName(const QString& name)
{
    m_name = name;
}

bool Project::isActive() const
{
    return m_isActive;
}

void Project::setActive(bool active)
{
    m_isActive = active;
}
