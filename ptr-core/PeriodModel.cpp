#include "PeriodModel.h"


#include "DatabaseManager.h"
#include "ProjectModel.h"

using namespace std;

PeriodModel::PeriodModel(QObject* parent) :
    QAbstractTableModel(parent),
    m_db(DatabaseManager::instance())
{
}

QModelIndex PeriodModel::addPeriod(const Period& period)
{
    auto activeProject = m_db.projectDao.activeProject();

    if (!activeProject)
        return QModelIndex();

    int rows = rowCount();
    beginInsertRows(QModelIndex(), rows, rows);

    Period newPeriod(period);
    m_db.periodDao.addPeriodInProject(*activeProject, newPeriod);
    m_periods.push_back(newPeriod);

    endInsertRows();
    return index(rows, 0);
}

void PeriodModel::loadPeriods(QDateTime from, QDateTime to, int projectId)
{
    beginResetModel();

    if (projectId <= 0) {
        m_periods.clear();
        return;
    }

    m_periods = m_db.periodDao.periods(from, to, projectId);

    endResetModel();
}

int PeriodModel::rowCount(const QModelIndex& /*parent*/) const
{
    return static_cast<int>(m_periods.size());
}

int PeriodModel::columnCount(const QModelIndex& /*parent*/) const
{
    return 5;
}

QVariant PeriodModel::data(const QModelIndex& index, int role) const
{
    if (!isIndexValid(index))
        return QVariant();

    auto row = static_cast<size_t>(index.row());

    if (role != Qt::DisplayRole)
        return QVariant();

    switch (index.column()) {
    case PeriodBeginColumn:
        return m_periods[row].periodBegin().toString("dd.MM.yy hh:mm:ss");
    case PeriodEndColumn:
        return m_periods[row].periodEnd().toString("dd.MM.yy hh:mm:ss");;
    case PeriodTimeColumn:
    {
        auto secondCount = static_cast<uint>(m_periods[row].periodBegin().secsTo(m_periods[row].periodEnd()));
        return QDateTime::fromTime_t(secondCount).toUTC().toString("hh:mm:ss");
    }
    case ProcessNameColumn:
        return m_periods[row].processName();
    case WindowTitleColumn:
        return m_periods[row].windowTitle();
    }

    return QVariant();
}

bool PeriodModel::removeRows(int row, int count, const QModelIndex& parent)
{
    if (row < 0 || row >= rowCount() || count < 0 || (row + count) > rowCount())
        return false;

    beginRemoveRows(parent, row, row + count - 1);

    int countLeft = count;
    while(countLeft--)
        m_db.periodDao.removePeriod(m_periods[static_cast<size_t>(row + countLeft)].id());

    m_periods.erase(m_periods.begin() + row, m_periods.begin() + row + count);

    endRemoveRows();

    return true;
}

QVariant PeriodModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole && orientation == Qt::Horizontal) {
        switch (section) {
        case PeriodBeginColumn:
            return QString(tr("Period begin"));
        case PeriodEndColumn:
            return QString(tr("Period end"));
        case PeriodTimeColumn:
            return QString(tr("Period time"));
        case ProcessNameColumn:
            return QString(tr("Process name"));
        case WindowTitleColumn:
            return QString(tr("Window title"));
        }
    }
    return QVariant();
}

bool PeriodModel::isIndexValid(const QModelIndex& index) const
{
    return !(index.row() < 0 || index.row() >= rowCount() || index.column() < 0 || index.column() >= columnCount() || !index.isValid());
}
