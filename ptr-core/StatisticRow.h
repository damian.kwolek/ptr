#ifndef STATISTICROW_H
#define STATISTICROW_H

#include <QDateTime>

class StatisticRow
{
public:
    StatisticRow();
    StatisticRow(const StatisticRow& r) = default;
    StatisticRow(StatisticRow&& r) = default;
    explicit StatisticRow(QString groupTxt, unsigned count, unsigned sum, float avg);

    QString groupTxt() const {return m_groupTxt;}
    unsigned count() const {return m_count;}
    unsigned sum() const {return m_sum;}
    float avg() const {return m_avg;}

private:
    QString m_groupTxt;
    unsigned m_count;
    unsigned m_sum;
    float m_avg;
};

#endif // STATISTICROW_H
