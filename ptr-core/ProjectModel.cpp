#include "ProjectModel.h"
#include <QIcon>

using namespace std;

ProjectModel::ProjectModel(QObject* parent /*=nullptr*/) :
    QAbstractListModel(parent),
    m_db(DatabaseManager::instance()),
    m_projects(m_db.projectDao.projects()),
    m_setActiveEnable(true)
{
}

ProjectModel::~ProjectModel()
{
}

QModelIndex ProjectModel::addProject(const Project& project)
{
    int rowIndex = rowCount();
    beginInsertRows(QModelIndex(), rowIndex, rowIndex);
    Project newProject(project);
    m_db.projectDao.addProject(newProject);
    m_projects.push_back(newProject);
    endInsertRows();
    return index(rowIndex, 0);
}

void ProjectModel::setActiveProject(const QModelIndex &index)
{
    clearActiveProject();
    setData(index, 1, Roles::IsActiveRole);
}

std::optional<QModelIndex> ProjectModel::activeProjectIndex() const
{
    auto findResult = std::find_if(m_projects.begin(), m_projects.end(), [](const Project& project){ return project.isActive();});

    if (findResult != m_projects.end())
        return index(static_cast<int>(std::distance(m_projects.begin(), findResult)), 0);
    else
        return {};
}

std::optional<int> ProjectModel::activeProjectId() const
{
    if (auto activeProject = activeProjectIndex())
        return projectId(*activeProject);
    else return {};
}

std::optional<int> ProjectModel::projectId(const QModelIndex &index) const
{
    if (!isIndexValid(index))
        return {};
    auto row = static_cast<size_t>(index.row());
    return m_projects[row].id();
}

int ProjectModel::rowCount(const QModelIndex& parent) const
{
    Q_UNUSED(parent);
    return static_cast<int>(m_projects.size());
}

QVariant ProjectModel::data(const QModelIndex& index, int role) const
{
    if (!isIndexValid(index))
        return QVariant();

    auto rowIndex = static_cast<size_t>(index.row());

    switch (role) {
        case Roles::IdRole:
            return m_projects[rowIndex].id();
        case Roles::NameRole:
        case Qt::DisplayRole:
            return m_projects[rowIndex].name();
        case Roles::IsActiveRole:
        case Qt::DecorationRole:
            if (m_projects[rowIndex].isActive())
                return QIcon(":/icons/project-active.png");
            return QIcon(":/icons/project-inactive.png");
        default:
            return QVariant();
    }
}

bool ProjectModel::setData(const QModelIndex& index, const QVariant& value, int role)
{
    if (!isIndexValid(index) || (role != Roles::NameRole && role != Roles::IsActiveRole))
        return false;

    auto rowIndex = static_cast<size_t>(index.row());

    if (role == Roles::NameRole) {
        m_projects[rowIndex].setName(value.toString());
        m_db.projectDao.updateProjectName(m_projects[rowIndex]);
    }
    else if (role == Roles::IsActiveRole) {
        m_projects[rowIndex].setActive(value.toBool());
        m_db.projectDao.updateActiveProject(m_projects[rowIndex]);
    }

    emit dataChanged(index, index);
    return true;
}

bool ProjectModel::removeRows(int row, int count, const QModelIndex& parent)
{
    if (rowCount() < 2 || row < 0 || row >= rowCount() || count < 0 || (row + count) > rowCount())
        return false;

    beginRemoveRows(parent, row, row + count - 1);

    int countLeft = count;

    while (countLeft--)
        m_db.projectDao.removeProject(m_projects[static_cast<size_t>(row + countLeft)].id());

    m_projects.erase(m_projects.begin() + row, m_projects.begin() + row + count);

    endRemoveRows();

    return true;
}

void ProjectModel::trackingChanged(bool tracking)
{
    m_setActiveEnable = !tracking;
}

void ProjectModel::clearActiveProject()
{
    auto activeProject = activeProjectIndex();

    if (!activeProject)
        return;

    setData(*activeProject, 0, Roles::IsActiveRole);

    emit dataChanged(*activeProject, *activeProject);
}

bool ProjectModel::isIndexValid(const QModelIndex& index) const
{
    if (index.row() < 0 || index.row() >= rowCount() || !index.isValid())
        return false;

    return true;
}

