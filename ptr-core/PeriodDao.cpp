#include "PeriodDao.h"

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QVariant>

#include "Period.h"
#include "StatisticRow.h"
#include "DatabaseManager.h"


using namespace std;

PeriodDao::PeriodDao(QSqlDatabase& database) :
    m_database(database)
{
}

void PeriodDao::init() const
{
    if (m_database.tables().contains("periods"))
        return;

    QSqlQuery query(m_database);
    query.exec(QString("CREATE TABLE periods")
    + " (id INTEGER PRIMARY KEY AUTOINCREMENT, "
    + "project_id INTEGER, "
    + "process_name TEXT, "
    + "window_title TEXT, "
    + "period_begin DATETIME, "
    + "period_end DATETIME)");
    DatabaseManager::debugQuery(query);
}

void PeriodDao::addPeriodInProject(int projectId, Period& period) const
{
    QSqlQuery query(m_database);
    query.prepare(QString("INSERT INTO periods")
        + " (project_id, process_name, window_title, period_begin, period_end)"
        + " VALUES ("
        + ":project_id, "
        + ":process_name, "
        + ":window_title, "
        + ":period_begin, "
        + ":period_end)");
    query.bindValue(":project_id", projectId);
    query.bindValue(":process_name", period.processName());
    query.bindValue(":window_title", period.windowTitle());
    query.bindValue(":period_begin", period.periodBegin());
    query.bindValue(":period_end", period.periodEnd());
    query.exec();
    DatabaseManager::debugQuery(query);
    period.setId(query.lastInsertId().toInt());
    period.setProjectId(projectId);
}

void PeriodDao::removePeriod(int id) const
{
    QSqlQuery query(m_database);
    query.prepare("DELETE FROM periods WHERE id = (:id)");
    query.bindValue(":id", id);
    query.exec();
    DatabaseManager::debugQuery(query);
}

void PeriodDao::removePeriodsForProject(int projectId) const
{
    QSqlQuery query(m_database);
    query.prepare("DELETE FROM periods WHERE project_id = (:project_id)");
    query.bindValue(":project_id", projectId);
    query.exec();
    DatabaseManager::debugQuery(query);
}

std::vector<Period> PeriodDao::periods(QDateTime from, QDateTime to, int projectId) const
{
    QSqlQuery query(m_database);
    query.prepare(R"(SELECT * FROM periods )"
                  R"(WHERE project_id = (:project_id) AND )"
                  R"((period_begin BETWEEN (:from) AND (:to)) AND )"
                  R"((period_end BETWEEN (:from) AND (:to)) )");

    query.bindValue(":project_id", projectId);
    query.bindValue(":from", from);
    query.bindValue(":to", to);

    query.exec();

    DatabaseManager::debugQuery(query);

    vector<Period> periods;
    while(query.next())
        periods.emplace_back(query.value("id").toInt(), query.value("project_id").toInt(),
                             query.value("process_name").toString(), query.value("window_title").toString(),
                             query.value("period_begin").toDateTime(), query.value("period_end").toDateTime());

    return periods;
}

std::vector<StatisticRow> PeriodDao::statistics(QDateTime from, QDateTime to, int projectId, int groupBy) const
{
    QString queryTxt;

    if (groupBy == 1) {
        queryTxt = R"(SELECT process_name AS group_txt, )"
                   R"(COUNT(*) AS count, )"
                   R"(SUM(CAST(STRFTIME('%s', period_end) AS INTEGER) - CAST(STRFTIME('%s', period_begin) AS INTEGER)) AS sum )"
                   R"(FROM periods )"
                   R"(WHERE project_id = (:project_id) AND )"
                   R"((period_begin BETWEEN (:from) AND (:to)) AND )"
                   R"((period_end BETWEEN (:from) AND (:to)) )"
                   R"(GROUP BY process_name)";
    }
    else if (groupBy == 2) {
        queryTxt = R"(SELECT (process_name || ' | ' || window_title) AS group_txt, )"
                   R"(COUNT(*) AS count, )"
                   R"(SUM(CAST(STRFTIME('%s', period_end) AS INTEGER) - CAST(STRFTIME('%s', period_begin) AS INTEGER)) AS sum )"
                   R"(FROM periods )"
                   R"(WHERE project_id = (:project_id) AND )"
                   R"((period_begin BETWEEN (:from) AND (:to)) AND )"
                   R"((period_end BETWEEN (:from) AND (:to)) )"
                   R"(GROUP BY process_name, window_title)";
    }
    else
        return {};

    QSqlQuery query(m_database);
    query.prepare(queryTxt);
    query.bindValue(":project_id", projectId);
    query.bindValue(":from", from);
    query.bindValue(":to", to);
    query.exec();

    DatabaseManager::debugQuery(query);

    vector<StatisticRow> result;

    while(query.next()) {
        auto count = query.value("count").toUInt();
        auto sum = query.value("sum").toUInt();
        float avg = count == 0 ? 0.0f : static_cast<float>(sum)/ static_cast<float>(count);
        result.emplace_back(query.value("group_txt").toString(), count, sum, avg);
    }

    return result;
}
