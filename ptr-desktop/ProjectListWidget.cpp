#include "ProjectListWidget.h"
#include "ui_ProjectListWidget.h"

#include <QInputDialog>

#include "ProjectModel.h"

ProjectListWidget::ProjectListWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ProjectListWidget),
    m_projectModel(nullptr)
{
    ui->setupUi(this);

    connect(ui->createProjectButton, &QPushButton::clicked,
            this, &ProjectListWidget::createProject);
}

ProjectListWidget::~ProjectListWidget()
{
    delete ui;
}

void ProjectListWidget::setModel(ProjectModel* model)
{
    m_projectModel = model;
    ui->projectList->setModel(m_projectModel);
}

void ProjectListWidget::setSelectionModel(QItemSelectionModel* selectionModel)
{
    ui->projectList->setSelectionModel(selectionModel);
}

void ProjectListWidget::createProject()
{
    if(!m_projectModel)
        return;

    bool ok;
    QString projectName = QInputDialog::getText(this, tr("Create a new project"), tr("Choose a name"), QLineEdit::Normal, tr("New project"), &ok);

    if (ok && !projectName.isEmpty()) {
        QModelIndex createdIndex = m_projectModel->addProject(Project(projectName));
        ui->projectList->setCurrentIndex(createdIndex);
    }
}
