#ifndef ACTIVEWINDOWTRACKER_H
#define ACTIVEWINDOWTRACKER_H

#include "PeriodModel.h"

#include <X11/Xlib.h>
#include <string>
#include <thread>

int ErrorHandler(Display* , XErrorEvent* );

class ActiveWindowTracker
{
public:
    ActiveWindowTracker(PeriodModel& periodModel);
    ~ActiveWindowTracker();


    QString getProcessName(int pid);


    QString getWindowName(Window window);
    QString getWindowOrAncestorName(Window window);
    int getWindowPid(Window window);
    int getWindowOrAncestorPid(Window window);
    void run();
    void stop();
    void track();

    PeriodModel& m_periodModel;
    Display* m_display = nullptr;
    std::thread m_thread;
    bool m_stopThread = false;
    Atom m_netWmNameAtom;
    Atom m_netWmPidAtom;
    Atom m_netWmUserTime;
};

#endif // ACTIVEWINDOWTRACKER_H
