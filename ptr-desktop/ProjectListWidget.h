#ifndef PROJECTLISTWIDGET_H
#define PROJECTLISTWIDGET_H

#include <QWidget>
#include <QItemSelectionModel>

namespace Ui {
class ProjectListWidget;
}

class ProjectModel;

class ProjectListWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ProjectListWidget(QWidget *parent = 0);
    ~ProjectListWidget();

    void setModel(ProjectModel* model);
    void setSelectionModel(QItemSelectionModel* selectionModel);

private slots:
    void createProject();

private:
    Ui::ProjectListWidget* ui;
    ProjectModel* m_projectModel;
};

#endif // PROJECTLISTWIDGET_H
