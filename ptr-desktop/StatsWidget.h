#ifndef STATSWIDGET_H
#define STATSWIDGET_H

#include <QWidget>

namespace Ui {
class StatsWidget;
}

class StatsModel;
class PeriodModel;
class ProjectModel;

class StatsWidget : public QWidget
{
    Q_OBJECT

public:
    explicit StatsWidget(QWidget *parent = nullptr);
    ~StatsWidget();

    void setModels(PeriodModel* periodModel, StatsModel* statsModel, ProjectModel* projectModel);

public slots:
    void groupByChanged(int index);
    void refresh();

private:
    Ui::StatsWidget *ui;
    int m_groupBy;
    StatsModel* m_statsModel;
    PeriodModel* m_periodModel;
    ProjectModel* m_projectModel;
};

#endif // STATSWIDGET_H
