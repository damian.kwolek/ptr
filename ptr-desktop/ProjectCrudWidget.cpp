#include "ProjectCrudWidget.h"
#include "ui_ProjectCrudWidget.h"

#include <QItemSelectionModel>

ProjectCrudWidget::ProjectCrudWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ProjectCrudWidget)
{
    ui->setupUi(this);
    ui->projectListWidget->setMaximumWidth(250);

    connect(this, SIGNAL(toggleTracking()), ui->projectWidget, SLOT(trackingToggled()));
}

ProjectCrudWidget::~ProjectCrudWidget()
{
    delete ui;
}

void ProjectCrudWidget::setModels(ProjectModel *projectModel, PeriodModel *periodModel)
{
    ui->projectListWidget->setModel(projectModel);
    ui->projectWidget->setProjectModel(projectModel);
    ui->projectWidget->setPeriodModel(periodModel);
}

void ProjectCrudWidget::setProjectSelectionModel(QItemSelectionModel* projectSelectionModel)
{
    ui->projectListWidget->setSelectionModel(projectSelectionModel);
    ui->projectWidget->setProjectSelectionModel(projectSelectionModel);
}
