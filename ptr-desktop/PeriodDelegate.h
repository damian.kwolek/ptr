#ifndef PERIODDELEGATE_H
#define PERIODDELEGATE_H

#include <QStyledItemDelegate>

class PeriodDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    PeriodDelegate(QObject* parent = nullptr);

    void paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const override;
    QSize sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const override;
};

#endif // PERIODDELEGATE_H
