#ifndef PERIODWIDGET_H
#define PERIODWIDGET_H

#include <QWidget>
#include <QItemSelection>

namespace Ui {
class PeriodWidget;
}

class PeriodModel;
class QItemSelectionModel;
class ThumbnailProxyModel;

class PeriodWidget : public QWidget
{
    Q_OBJECT

public:
    explicit PeriodWidget(QWidget *parent = nullptr);
    ~PeriodWidget() override;
    void setModel(ThumbnailProxyModel* model);
    void setSelectionModel(QItemSelectionModel* selectionModel);

signals:
    void backToGallery();

protected:
    void resizeEvent(QResizeEvent* event) override;

private slots:
    void deletePeriod();
    void loadPeriod(const QItemSelection& selected);

private:
    void updatePeriodPixmap();

private:
    Ui::PeriodWidget* ui;
    ThumbnailProxyModel* m_model;
    QItemSelectionModel* m_selectionModel;
    QPixmap m_pixmap;
};

#endif // PERIODWIDGET_H
