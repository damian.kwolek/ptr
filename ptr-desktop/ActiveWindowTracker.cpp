
#include "ActiveWindowTracker.h"
#include "Period.h"

#include <fstream>

using namespace std;

ActiveWindowTracker::ActiveWindowTracker(PeriodModel &periodModel):
    m_periodModel(periodModel),
    m_display(XOpenDisplay(nullptr)),
    m_netWmNameAtom(XInternAtom(m_display, "_NET_WM_NAME", False)),
    m_netWmPidAtom(XInternAtom(m_display, "_NET_WM_PID", False)),
    m_netWmUserTime(XInternAtom(m_display, "_NET_WM_USER_TIME", False))
{

}

ActiveWindowTracker::~ActiveWindowTracker()
{
    m_stopThread = true;

    if (m_thread.joinable())
        m_thread.join();

    XCloseDisplay(m_display);
}

QString ActiveWindowTracker::getProcessName(int pid)
{
    string name;

    ifstream commFile("/proc/" + to_string(pid) + "/comm");

    if (commFile)
        std::getline(commFile, name);

    commFile.close();

    return name.c_str();
}

QString ActiveWindowTracker::getWindowName(Window window)
{
    if (window < 1)
        return {};

    Atom actualType;
    int actualFormat;
    unsigned long nItems, bytesAfter;
    unsigned char* prop = nullptr;

    XGetWindowProperty(m_display, window, m_netWmNameAtom, 0, (~0L),
                       False, AnyPropertyType, &actualType,
                       &actualFormat, &nItems, &bytesAfter, &prop);

    QString name = nItems > 0 ? string(reinterpret_cast<char*>(prop), nItems).c_str() : "";

    XFree(prop);

    return name;
}

QString ActiveWindowTracker::getWindowOrAncestorName(Window window)
{
    if (window < 1)
        return {};

    auto name = getWindowName(window);

    if(!name.isEmpty())
        return name;

    Window root, parent, currentWindow = window;
    Window* children = nullptr;
    unsigned nChildren = 0;

    while (name.isEmpty()) {
        XQueryTree(m_display, currentWindow, &root,
                    &parent, &children, &nChildren);

        XFree(children);

        if(parent == 0)
            break;

        currentWindow = parent;

        name = getWindowName(currentWindow);

        if(root == parent)
            break;
    }
    return name;
}

int ActiveWindowTracker::getWindowPid(Window window)
{
    if (window < 1)
        return 0;

    Atom actualType;
    int actual_format_return;
    unsigned long nItems, bytesAfter;
    unsigned char* prop = nullptr;

    XGetWindowProperty(m_display, window,
                       m_netWmPidAtom, 0, (~0L),
                       False, AnyPropertyType, &actualType,
                       &actual_format_return, &nItems,
                       &bytesAfter, &prop);

    int pid = nItems > 0 ? *(reinterpret_cast<int*>(prop)) : -1;

    XFree(prop);

    return pid;
}

int ActiveWindowTracker::getWindowOrAncestorPid(Window window)
{
    auto pid = getWindowPid(window);

    if (pid > 0)
        return pid;

    Window root, parent, currentWindow = window;
    Window* children = nullptr;
    unsigned nChildren = 0;

    while (pid < 1) {
        XQueryTree(m_display, currentWindow, &root,
                    &parent, &children, &nChildren);

        XFree(children);

        if(parent == 0)
            break;

        currentWindow = parent;

        pid = getWindowPid(currentWindow);

        if (root == parent)
            break;
    }

    return pid;
}

void ActiveWindowTracker::run()
{
    m_stopThread = false;
    m_thread = std::thread(&ActiveWindowTracker::track, this);
}

void ActiveWindowTracker::stop()
{
    m_stopThread = true;

    if (m_thread.joinable())
        m_thread.join();
}

void ActiveWindowTracker::track()
{
    if(!m_display)
        return;

    Window focusWindow;
    int focusState = RevertToNone;

    XGetInputFocus(m_display, &focusWindow, &focusState);

    auto eventMask = FocusChangeMask | PropertyChangeMask;

    XSelectInput(m_display, focusWindow, eventMask);

    auto currWindowName = getWindowOrAncestorName(focusWindow);
    auto lastWindowName = currWindowName;
    auto currPid = getWindowOrAncestorPid(focusWindow);
    auto lastPid = currPid;
    auto lastProcessName = getProcessName(currPid);
    auto currProcessName = lastProcessName;
    auto currTime = QDateTime::currentDateTime();
    auto lastTime = currTime;

    XSetErrorHandler(ErrorHandler);

    while (!m_stopThread) {
        XEvent e;

        if(!XCheckWindowEvent(m_display, focusWindow, eventMask, &e)) {
            std::this_thread::sleep_for(std::chrono::milliseconds(250));
            continue;
        }

        if(e.type == FocusOut ||
            (e.type == PropertyNotify && e.xproperty.state == PropertyNewValue &&
            (e.xproperty.atom == m_netWmNameAtom || e.xproperty.atom == m_netWmUserTime)))
        {
            XGetInputFocus(m_display, &focusWindow, &focusState);

            XSelectInput(m_display, focusWindow, eventMask);

            currWindowName = getWindowOrAncestorName(focusWindow);

            currPid = getWindowOrAncestorPid(focusWindow);

            bool processChanged = currPid != lastPid;

            if (processChanged)
            {
                currProcessName = getProcessName(currPid);

                if(currProcessName == lastProcessName)
                    processChanged = false;
            }

            if((processChanged || currWindowName != lastWindowName) && !currProcessName.isEmpty())
            {
                currTime = QDateTime::currentDateTime();
                m_periodModel.addPeriod(Period(lastProcessName, lastWindowName, lastTime, currTime));
                lastProcessName = currProcessName;
            }

            lastWindowName = currWindowName;
            lastPid = currPid;
            lastTime = currTime;
        }
    }

    m_periodModel.addPeriod(Period(lastProcessName, lastWindowName, lastTime, QDateTime::currentDateTime()));
}

int ErrorHandler(Display *, XErrorEvent *)
{
    return 0;
}
