#-------------------------------------------------
#
# Project created by QtCreator 2019-06-16T15:09:10
#
#-------------------------------------------------

QT       += core gui widgets

TARGET = ptr-desktop
TEMPLATE = app
CONFIG += c++17

SOURCES += main.cpp\
        MainWindow.cpp \
    ProjectListWidget.cpp \
    ProjectWidget.cpp \
    ActiveWindowTracker.cpp \
    ProjectCrudWidget.cpp \
    StatsWidget.cpp

HEADERS  += MainWindow.h \
    ProjectListWidget.h \
    ProjectWidget.h \
    ActiveWindowTracker.h \
    ProjectCrudWidget.h \
    StatsWidget.h

FORMS    += MainWindow.ui \
    ProjectListWidget.ui \
    ProjectWidget.ui \
    ProjectCrudWidget.ui \
    StatsWidget.ui

LIBS += -L$$OUT_PWD/../ptr-core/ -lptr-core -lX11

INCLUDEPATH += $$PWD/../ptr-core
DEPENDPATH += $$PWD/../ptr-core

RESOURCES += \
    res/resource.qrc
