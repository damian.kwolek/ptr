#ifndef PROJECTWIDGET_H
#define PROJECTWIDGET_H

#include <QWidget>
#include <QModelIndex>

namespace Ui {
class ProjectWidget;
}

class ProjectModel;
class PeriodModel;
class QItemSelectionModel;

class ProjectWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ProjectWidget(QWidget *parent = nullptr);
    ~ProjectWidget();

    void setProjectModel(ProjectModel* projectModel);
    void setProjectSelectionModel(QItemSelectionModel* projectSelectionModel);
    void setPeriodModel(PeriodModel* periodModel);

public slots:
    void trackingToggled();

private slots:
    void deleteProject();
    void editProject();
    void setActiveProject();

private:
    void clearUi();
    void loadProject(const QModelIndex& projectIndex);

    Ui::ProjectWidget* ui;
    ProjectModel* m_projectModel;
    PeriodModel* m_periodModel;
    QItemSelectionModel* m_projectSelectionModel;
    bool m_tracking;
};

#endif // PROJECTWIDGET_H
