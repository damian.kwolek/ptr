#include "StatsWidget.h"
#include "ui_StatsWidget.h"
#include "StatsModel.h"
#include "PeriodModel.h"
#include "ProjectModel.h"

StatsWidget::StatsWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::StatsWidget),
    m_statsModel(nullptr),
    m_projectModel(nullptr)
{
    ui->setupUi(this);
}

StatsWidget::~StatsWidget()
{
    delete ui;
}

void StatsWidget::setModels(PeriodModel *periodModel, StatsModel *statsModel, ProjectModel *projectModel)
{
//    if ((m_statsModel = statsModel))
//        ui->statsTableView->setModel(m_statsModel);
    m_statsModel = statsModel;

    if ((m_periodModel = periodModel))
        ui->statsTableView->setModel(m_periodModel);

    ui->statsTableView->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
    ui->statsTableView->horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeToContents);
    ui->statsTableView->horizontalHeader()->setSectionResizeMode(2, QHeaderView::ResizeToContents);
    ui->statsTableView->horizontalHeader()->setSectionResizeMode(3, QHeaderView::ResizeToContents);
    ui->statsTableView->horizontalHeader()->setSectionResizeMode(4, QHeaderView::Stretch);



    ui->statsTableView->horizontalHeader()->setSectionsClickable(false);

    if ((m_projectModel = projectModel))
        ui->projectComboBox->setModel(m_projectModel);

    ui->groupByComboBox->addItem(tr("None"));
    ui->groupByComboBox->addItem(tr("Process name"));
    ui->groupByComboBox->addItem(tr("Process name + window title"));

    ui->toDateTimeEdit->setDateTime(QDateTime::currentDateTime());

    connect(ui->groupByComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(groupByChanged(int)));
    connect(ui->refreshButton, SIGNAL(clicked()), this, SLOT(refresh()));
}

void StatsWidget::groupByChanged(int index)
{
    m_groupBy = index;
}

void StatsWidget::refresh()
{
    if (ui->groupByComboBox->currentIndex()) {
        ui->statsTableView->setModel(m_statsModel);

        if (auto projectId = m_projectModel->projectId(m_projectModel->index(ui->projectComboBox->currentIndex(), 0)))
            m_statsModel->loadStatistics(ui->fromDateTimeEdit->dateTime(),
                                         ui->toDateTimeEdit->dateTime(),
                                         *projectId,
                                         ui->groupByComboBox->currentIndex());

    ui->statsTableView->horizontalHeader()->setSectionResizeMode(3, QHeaderView::Stretch);
    }
    else {

        ui->statsTableView->setModel(m_periodModel);

        if (auto projectId = m_projectModel->projectId(m_projectModel->index(ui->projectComboBox->currentIndex(), 0)))
            m_periodModel->loadPeriods(ui->fromDateTimeEdit->dateTime(), ui->toDateTimeEdit->dateTime(), *projectId);

        ui->statsTableView->horizontalHeader()->setSectionResizeMode(3, QHeaderView::ResizeToContents);
        ui->statsTableView->horizontalHeader()->setSectionResizeMode(4, QHeaderView::Stretch);
    }

    ui->statsTableView->update();
}
