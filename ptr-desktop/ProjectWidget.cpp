#include "ProjectWidget.h"
#include "ui_ProjectWidget.h"

#include <QInputDialog>
#include <QFileDialog>
#include <QItemSelectionModel>
#include <QMessageBox>

#include "ProjectModel.h"
#include "PeriodModel.h"

ProjectWidget::ProjectWidget(QWidget *parent /*=nullptr*/) :
    QWidget(parent),
    ui(new Ui::ProjectWidget),
    m_projectModel(nullptr),
    m_periodModel(nullptr),
    m_projectSelectionModel(nullptr),
    m_tracking(false)
{
    ui->setupUi(this);
    clearUi();

    connect(ui->deleteButton, &QPushButton::clicked, this, &ProjectWidget::deleteProject);
    connect(ui->editButton, &QPushButton::clicked, this, &ProjectWidget::editProject);
    connect(ui->setActiveButton, &QPushButton::clicked, this, &ProjectWidget::setActiveProject);
}

ProjectWidget::~ProjectWidget()
{
    delete ui;
}

void ProjectWidget::setProjectModel(ProjectModel* projectModel)
{
    m_projectModel = projectModel;

    connect(m_projectModel, &QAbstractItemModel::dataChanged, [this] (const QModelIndex &topLeft) {
        if (topLeft == m_projectSelectionModel->currentIndex()) {
            loadProject(topLeft);
        }
    });
}

void ProjectWidget::setProjectSelectionModel(QItemSelectionModel* projectSelectionModel)
{
    m_projectSelectionModel = projectSelectionModel;

    connect(m_projectSelectionModel, &QItemSelectionModel::selectionChanged, [this](const QItemSelection &selected) {
        if (selected.isEmpty()) {
            clearUi();
            return;
        }
        loadProject(selected.indexes().first());
    });

    m_projectSelectionModel->select(m_projectModel->index(0,0), QItemSelectionModel::Select);
}

void ProjectWidget::setPeriodModel(PeriodModel *periodModel)
{
    m_periodModel = periodModel;
}

void ProjectWidget::trackingToggled()
{
    m_tracking = !m_tracking;
    auto currentSel = m_projectSelectionModel->selection();
    m_projectSelectionModel->selectionChanged(currentSel, currentSel);
}

void ProjectWidget::deleteProject()
{
    if (m_projectSelectionModel->selectedIndexes().isEmpty())
        return;

    auto questionResult = QMessageBox::question(this, tr("Delete project"), tr("Are you sure you want to delete the project?"));

    if (questionResult == QMessageBox::No)
        return;

    auto currentIndex = m_projectSelectionModel->currentIndex();

    if (auto projectId = m_projectModel->projectId(currentIndex))
        DatabaseManager::instance().periodDao.removePeriodsForProject(*projectId);

    m_projectModel->removeRow(currentIndex.row());

    QModelIndex previousModelIndex = m_projectModel->index(currentIndex.row() - 1, 0);

    m_projectSelectionModel->setCurrentIndex(previousModelIndex, QItemSelectionModel::SelectCurrent);


    if(previousModelIndex.isValid()) {
        m_projectSelectionModel->setCurrentIndex(previousModelIndex, QItemSelectionModel::SelectCurrent);
        return;
    }

    QModelIndex nextModelIndex = m_projectModel->index(currentIndex.row(), 0);

    if(nextModelIndex.isValid()) {
        m_projectSelectionModel->setCurrentIndex(nextModelIndex, QItemSelectionModel::SelectCurrent);
        return;
    }
}

void ProjectWidget::editProject()
{
    if (m_projectSelectionModel->selectedIndexes().isEmpty())
        return;

    QModelIndex currentProjectIndex = m_projectSelectionModel->selectedIndexes().first();
    QString oldProjectName = m_projectModel->data(currentProjectIndex, ProjectModel::Roles::NameRole).toString();

    bool ok;
    QString newName = QInputDialog::getText(this, tr("Project's name"), tr("Change project name"), QLineEdit::Normal, oldProjectName, &ok);

    if (ok && !newName.isEmpty())
        m_projectModel->setData(currentProjectIndex, newName, ProjectModel::Roles::NameRole);
}

void ProjectWidget::clearUi()
{
    ui->projectName->setText("");
    ui->deleteButton->setVisible(false);
    ui->editButton->setVisible(false);
    ui->setActiveButton->setVisible(false);
}

void ProjectWidget::loadProject(const QModelIndex& projectIndex)
{
    ui->projectName->setText(m_projectModel->data(projectIndex, Qt::DisplayRole).toString());
    ui->deleteButton->setVisible(true);
    ui->editButton->setVisible(true);
    ui->setActiveButton->setVisible(true);
    bool isProjectActive = m_projectModel->activeProjectIndex() == projectIndex;
    ui->deleteButton->setDisabled(isProjectActive);
    ui->setActiveButton->setDisabled(isProjectActive || m_tracking);
}

void ProjectWidget::setActiveProject()
{
    if (m_projectSelectionModel->selectedIndexes().isEmpty())
        return;

    QModelIndex currentProjectIndex = m_projectSelectionModel->selectedIndexes().first();
    m_projectModel->setActiveProject(currentProjectIndex);
}
