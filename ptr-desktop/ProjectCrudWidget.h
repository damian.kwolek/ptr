#ifndef PROJECTCRUDWIDGET_H
#define PROJECTCRUDWIDGET_H

#include <QWidget>

namespace Ui {
class ProjectCrudWidget;
}

class QItemSelectionModel;
class ProjectModel;
class PeriodModel;

class ProjectCrudWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ProjectCrudWidget(QWidget *parent = nullptr);
    ~ProjectCrudWidget();

    void setModels(ProjectModel* projectModel, PeriodModel* periodModel);
    void setProjectSelectionModel(QItemSelectionModel* projectSelectionModel);


signals:
    void periodActivated(const QModelIndex& index);
    void toggleTracking();

private:
    Ui::ProjectCrudWidget *ui;
};

#endif // PROJECTCRUDWIDGET_H
