#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStackedWidget>
#include <memory>

namespace Ui {
class MainWindow;
}

class ProjectCrudWidget;
class PeriodWidget;
class PeriodModel;
class StatsModel;
class ActiveWindowTracker;
class StatsWidget;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void displayProjectsCrud();
    void displayStats();
    void toggleTracking();

signals:
    void trackingChanged(bool tracking);

private:
    Ui::MainWindow *ui;
    ProjectCrudWidget* m_projectCrudWidget;
    StatsWidget* m_statsWidget;
    PeriodModel* m_periodModel;
    StatsModel* m_statsModel;
    QStackedWidget* m_stackedWidget;
    std::unique_ptr<ActiveWindowTracker> m_activeWndTracker;
    QAction* m_startStopTrackingAction;
    bool m_tracking;

};

#endif // MAINWINDOW_H
