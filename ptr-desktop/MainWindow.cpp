#include "MainWindow.h"
#include "ui_MainWindow.h"

#include <QStackedWidget>
#include <QItemSelectionModel>

#include "ProjectCrudWidget.h"
#include "StatsWidget.h"
#include "PeriodWidget.h"
#include "ProjectModel.h"
#include "PeriodModel.h"
#include "StatsModel.h"
#include "ActiveWindowTracker.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_projectCrudWidget(new ProjectCrudWidget(this)),
    m_statsWidget(new StatsWidget(this)),
    m_stackedWidget(new QStackedWidget(this)),
    m_startStopTrackingAction(nullptr),
    m_tracking(false)
{
    ui->setupUi(this);

    ProjectModel* projectModel = new ProjectModel(this);
    QItemSelectionModel* projectSelectionModel = new QItemSelectionModel(projectModel, this);
    m_periodModel= new PeriodModel(this);
    m_statsModel = new StatsModel(this);

    m_projectCrudWidget->setModels(projectModel, m_periodModel);
    m_projectCrudWidget->setProjectSelectionModel(projectSelectionModel);

    m_statsWidget->setModels(m_periodModel, m_statsModel, projectModel);

    m_activeWndTracker.reset(new ActiveWindowTracker(*m_periodModel));

    m_stackedWidget->addWidget(m_projectCrudWidget);
    m_stackedWidget->addWidget(m_statsWidget);
    displayProjectsCrud();

    setCentralWidget(m_stackedWidget);

    QToolBar* toolbar = ui->toolBar;

    QAction *projectCrudAction = new QAction("Projects");
    projectCrudAction->setCheckable(true);
    projectCrudAction->setChecked(true);

    QAction *statsAction = new QAction("Stats");
    statsAction->setCheckable(true);

    QActionGroup* actionsGroup = new QActionGroup(this);
    actionsGroup->addAction(projectCrudAction);
    actionsGroup->addAction(statsAction);

    toolbar->addActions(actionsGroup->actions());
    toolbar->addSeparator();

    m_startStopTrackingAction = new QAction("Start");
    m_startStopTrackingAction->setToolTip("Start tracking");
    toolbar->addAction(m_startStopTrackingAction);

    connect(projectCrudAction, SIGNAL(triggered()), this, SLOT(displayProjectsCrud()));
    connect(statsAction, SIGNAL(triggered()), this, SLOT(displayStats()));
    connect(m_startStopTrackingAction, SIGNAL(triggered()), this, SLOT(toggleTracking()));
    connect(m_startStopTrackingAction, SIGNAL(triggered()), m_projectCrudWidget, SIGNAL(toggleTracking()));

    //m_activeWndTracker->run();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::displayProjectsCrud()
{
    m_stackedWidget->setCurrentWidget(m_projectCrudWidget);
}

void MainWindow::displayStats()
{
    m_stackedWidget->setCurrentWidget(m_statsWidget);
}

void MainWindow::toggleTracking()
{
    if (m_tracking) {
        m_activeWndTracker->stop();
        m_startStopTrackingAction->setText("Start");
        m_startStopTrackingAction->setToolTip("Start tracking");
    }
    else {
        m_activeWndTracker->run();
        m_startStopTrackingAction->setText("Stop");
        m_startStopTrackingAction->setToolTip("Stop tracking");
    }
    m_tracking = !m_tracking;
}
